# PEC 1 - Un juego de aventuras

## Nota previa

Entiéndase el uso genérico de "el jugador" y "el enemigo" como inclusivo de "el jugador o la jugadora" y "el enemigo o la enemiga".

## FUNCIONAMIENTO

El juego empieza con la pantalla inicial (MenuPrincipal) donde aparecen dos botones:

- "Jugar", que, si lo pulsamos, comenzaremos una nueva partida, llevándonos a la pantalla del juego propiamente dicho (Juego).
- "Salir", que cierra la aplicación.

En la segunda pantalla (Juego), se elige aleatoriamente quién empieza el duelo.

- Si es el turno del jugador, aparece la lista de los insultos posibles para que éste elija uno. Cuando el jugador escoge un insulto, se elige una respuesta aleatoria para el enemigo. Si la respuesta es correcta, el asalto lo gana el enemigo y se incrementa en uno el número de asaltos ganados por él, indicado en la parte superior derecha de la pantalla. En cambio, si la respuesta dada por el enemigo es incorrecta, el asalto lo gana el jugador y se incrementa en uno, en este caso, el número de asaltos ganados por el jugador, indicado en la parte superior izquierda de la pantalla.

- Si es el turno del enemigo, se elige de forma aleatoria un insulto. Aparece entonces la lista de las respuestas posibles para que el jugador escoja una. Si la respuesta es correcta, el asalto lo gana el jugador y se incrementa en uno el número de asaltos ganados por el jugador, mostrado en la parte superior izquierda de la pantalla. Si por el contrario, la respuesta dada por el jugador no es correcta, el asalto lo gana el enemigo y se incrementa en uno el número de asaltos ganados por éste, indicado en la parte superior derecha de la pantalla.

Cuando uno de los dos jugadores consigue ganar tres asaltos, gana la partida y aparece la tercera pantalla (Final), donde se puede escuchar una música diferente según haya ganado el jugador o el enemigo, y aparece un mensaje ("¡HAS GANADO!" o "¡HAS PERDIDO!"). En esta pantalla, se puede elegir jugar otra partida ("Nueva partida") o volver al menú principal ("Menú principal").


## ESTRUCTURA

1. Creación de pantalla inicial (MenuPrincipal):
- Adición de fondo.
- Adición de música.
- Creación del botón Jugar. 
	i. Implementación de funcionalidad (abrir pantalla Juego – script NuevaPartida).
- Creación del botón Salir.
	i. Implementación de funcionalidad (salir de la aplicación – script SalirJuego). 

2. Creación de pantalla de juego (Juego).
- Adición de fondo.
- Adición de música.
- Creación de contenedor de frases del jugador.
	 i. Adición de botón para seleccionar las frases. 
	ii. Adición de barra de desplazamiento. 
- Creación de conversaciones de los jugadores.
- Creación de indicadores de asaltos ganados.
- Creación de estructura de datos de los insultos. Scripts Insultos y ListaInsultos.
- Lectura de los insultos de un fichero de texto / JSON.
- Creación de la lógica del juego. Script ControladorJuego.

3. Creación de pantalla final (Final). Script PantallaFinal.
- Adición de fondo.
- Adición de música.
	 i. Si el jugador ha ganado, suena “09 - Guybrush and Elaine”.
	ii. Si el jugador ha perdido, suena “04 - LeChuck's Theme”.
- Creación del botón Nueva partida. 
	i. Implementación de funcionalidad (abrir pantalla Juego – script NuevaPartida). 
- Creación del botón Menú principal. 
	i. Implementación de funcionalidad (abrir pantalla MenuPrincipal – script MenuPrincipal). 
- Creación de indicador de si el jugador ha ganado o perdido.




